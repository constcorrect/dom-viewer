# dom-viewer

A simple utility to view the DOM structure of an HTML page.
Uses ncurses terminal graphics library.
Uses build2 build system.

## Building

You need to have build2 installed. See [these instructions](https://build2.org/install.xhtml).
Next, see `build.sh` in the project's root directory.
Modify some variables in it if you need to.

## Usage

`dom-viewer <html file>`

Once the program starts, use vim keys (hjkl) to navigate the view.
Use 'i' key to toggle between list view and details view.
In the 'details' view, you can only see info about the selected node.
You can change any keybinding in the Config.hpp file.

## Motivation

I needed this tool to better understand the structure of HTML documents I need
to parse and how this structure is represented in libxml2.
I also wanted to learn the ncurses library which is used here via my
wrapper library to give it a more object-oriented feel.
