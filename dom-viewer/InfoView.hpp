#pragma once

#include "View.hpp"

class InfoView : public View {
public:
    explicit InfoView(Model* m, Window* w);
    void draw() override;

private:
    Window derived;
    FileLogger logger{"infoView.log"};
};
