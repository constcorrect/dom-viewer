#include <exception>
#include <iostream>

#include <mycurses/Core.mpp>
#include <mycurses/Window.mpp>

#include "Controller.hpp"
#include "InfoView.hpp"
#include "ListView.hpp"
#include "Model.hpp"

namespace nc = ncurses;

using std::cerr;
using std::exception;

void initCurses() {
    nc::initscr();
    nc::raw();
    nc::echo(false);
    nc::cbreak();
    nc::curs_set(0);
}

int main(int argc, char** argv) {
    if (argc != 2) {
        cerr << "Usage: " << argv[0] << " file.html\n";
        return -1;
    }

    try {
        Model model{argv[1]};
        initCurses();
        nc::Size maxSize = nc::Window::maxSize();
        nc::Window left{nc::Size{maxSize.y, maxSize.x / 3}, nc::Position{0, 0}};
        nc::Window middle{nc::Size{maxSize.y, maxSize.x / 3},
                          nc::Position{0, maxSize.x / 3}};
        nc::Window upperRight{nc::Size{maxSize.y / 2, maxSize.x / 3},
                              nc::Position{0, maxSize.x * 2 / 3}};
        nc::Window lowerRight{nc::Size{maxSize.y / 2, maxSize.x / 3},
                              nc::Position{maxSize.y / 2, maxSize.x * 2 / 3}};
        ListView previous{&model, &left, ListView::Pos::Previous,
                          "view-prev.log"};
        ListView current{&model, &middle, ListView::Pos::Current,
                         "view-current.log"};
        ListView next{&model, &upperRight, ListView::Pos::Next,
                      "view-next.log"};
        InfoView details{&model, &lowerRight};
        Controller controller{&model};
        controller.registerCallback(Controller::CallbackType::moved, [&]() {
            previous.updateMenu();
            next.updateMenu();
            current.updateMenu();
        });
        controller.registerCallback(Controller::CallbackType::selectionUpdated,
                                    [&]() {
                                        current.updateSelection();
                                        next.updateMenu();
                                    });
        while (!controller.isQuitSet()) {
            previous.draw();
            current.draw();
            next.draw();
            details.draw();
            controller.handleInput(middle.getchar());
        }
        ncurses::endwin();
    } catch (const exception& e) {
        ncurses::endwin();
        cerr << "Exception occurred: " << e.what() << '\n';
    } catch (...) {
        ncurses::endwin();
        cerr << "Unknown exception occurred";
    }

    return 0;
}
