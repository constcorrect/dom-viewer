#pragma once

#include <list>
#include <stdexcept>
#include <string>
#include <string_view>
#include <unordered_map>
#include <variant>
#include <vector>

#include <libxml/HTMLparser.h>
#include <libxml/xmlreader.h>

#include <cpp-utils/LazyMap.mpp>
#include <cpp-utils/Logger.mpp>
#include <libxml2-utils/Misc.mpp>
#include <libxml2-utils/Node.mpp>

struct Selection {
    Node node{nullptr};
    int index{0};
};

struct Attribute {
    std::string_view name;
    std::string_view value;
};

// TODO merge Info and Child classes ??
// TODO better naming

struct TextNodeInfo {
    std::string_view content;
};

struct ElementNodeInfo {
    std::string_view name;
    std::list<Attribute> attributes;
};

using NodeInfo = std::variant<TextNodeInfo, ElementNodeInfo>;

struct ElementChild {
    std::string_view name;
};

struct TextChild {};

using ChildInfo = std::variant<ElementChild, TextChild>;

class Model {
public:
    explicit Model(const char* fileName);
    Model(const Model& other) = delete;
    Model& operator=(const Model& other) = delete;
    Model(Model&& other) = default;
    Model& operator=(Model&& other) = default;
    ~Model();

    void selectPrevious() noexcept;
    void selectNext() noexcept;
    [[nodiscard]] auto moveInto() -> bool;
    [[nodiscard]] auto moveOut() -> bool;
    auto items() -> std::vector<ChildInfo>;
    auto itemsPrevious() -> std::vector<ChildInfo>;
    auto itemsNext() -> std::vector<ChildInfo>;
    auto selectionIndex() const noexcept -> int;
    auto previousSelectionIndex() const -> int;
    auto nextSelectionIndex() const -> int;
    auto selectedNodeFullInfo() -> NodeInfo;

private:
    auto nodeFullInfo(xmlNode* node) -> NodeInfo;
    auto itemsForNode(xmlNode* node) -> std::vector<ChildInfo>;
    auto extractProperties(xmlNode* node) -> std::list<Attribute>;
    auto findOr(xmlNode* node, Selection alternative) const -> Selection;
    auto static newItemsVector(xmlNode* node) -> std::vector<ChildInfo>;

private:
    using ItemsCache = LazyMap<xmlNode*, std::vector<ChildInfo>>;
    using AttributeValueCache =
        LazyMap<xmlAttr*, std::string,
                std::string (*)(xmlAttr const*, xmlDoc*)>;

    htmlDocPtr doc{nullptr};
    xmlNode* root{nullptr};
    xmlNode* currentNode{nullptr};
    Selection selection;
    std::unordered_map<xmlNode*, Selection> selectionMap;
    AttributeValueCache attributeValues{attributeValueToString};
    ItemsCache itemsCache{newItemsVector};
    mutable FileLogger logger{"model.log"};
};
