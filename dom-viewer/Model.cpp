#include "Model.hpp"

#include <list>
#include <stdexcept>
#include <string_view>
#include <vector>

#include <libxml/HTMLparser.h>
#include <libxml/xmlreader.h>

#include <cpp-utils/Algorithms.mpp>
#include <libxml2-utils/XmlIterator.mpp>

using std::list;
using std::logic_error;
using std::runtime_error;
using std::string_view;
using std::vector;

Model::Model(const char* fileName) {
    doc = htmlReadFile(fileName, nullptr,
                       HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR |
                           HTML_PARSE_NOWARNING | HTML_PARSE_NONET);
    if (doc == nullptr) {
        throw runtime_error{"Document not parsed successfully"};
    }

    root = xmlDocGetRootElement(doc);
    if (root == nullptr) {
        xmlFreeDoc(doc);
        xmlCleanupParser();
        throw runtime_error{"Empty document"};
    }
    currentNode = root;
    // TODO this is not safe: currentNode might not have children
    selection = Selection{Node{currentNode->children}};
}

Model::~Model() {
    if (doc != nullptr) {
        xmlFreeDoc(doc);
    }
    xmlCleanupParser();
}

void Model::selectPrevious() noexcept {
    if (!selection.node.hasPrevious()) {
        return;
    }
    selection.node = selection.node->prev;
    selection.index -= 1;
}

void Model::selectNext() noexcept {
    if (!selection.node.hasNext()) {
        return;
    }
    selection.node = selection.node->next;
    selection.index += 1;
}

[[nodiscard]] bool Model::moveInto() {
    if (!selection.node.hasChildren()) {
        return false;
    }
    selectionMap[currentNode] = selection;
    currentNode = selection.node;
    selection = findOr(currentNode, Selection{Node{currentNode->children}});
    return true;
}

[[nodiscard]] bool Model::moveOut() {
    if (!currentNode->parent || currentNode == root) {
        return false;
    }
    selectionMap[currentNode] = selection;
    currentNode = currentNode->parent;
    selection = selectionMap[currentNode];
    return true;
}

vector<ChildInfo> Model::items() { return itemsCache.getOrCreate(currentNode); }

vector<ChildInfo> Model::itemsPrevious() {
    if (!currentNode->parent || currentNode == root) {
        return {};
    }
    return itemsCache.getOrCreate(currentNode->parent);
}

vector<ChildInfo> Model::itemsNext() {
    if (!selection.node.hasChildren()) {
        return {};
    }
    return itemsCache.getOrCreate(selection.node);
}

int Model::selectionIndex() const noexcept { return selection.index; }

int Model::previousSelectionIndex() const {
    if (!currentNode->parent || currentNode == root) {
        return 0;
    }
    return selectionMap.at(currentNode->parent).index;
}

int Model::nextSelectionIndex() const {
    auto it = selectionMap.find(selection.node);
    return (it != selectionMap.end()) ? (*it).second.index : 0;
}

NodeInfo Model::selectedNodeFullInfo() { return nodeFullInfo(selection.node); }

// TODO make const?
NodeInfo Model::nodeFullInfo(xmlNode* node) {
    switch (node->type) {
    case XML_ELEMENT_NODE: {
        return ElementNodeInfo{toString(node->name), extractProperties(node)};
    }
    case XML_TEXT_NODE: {
        return TextNodeInfo{toString(node->content)};
    }
    default:
        logger.warn("Unknown node type: ", node->type, " on line ", __LINE__);
        throw logic_error{"No handler for this type of node"};
    }
}

// TODO store lists in a map too
list<Attribute> Model::extractProperties(xmlNode* node) {
    return transformed<list<Attribute>>(
        node->properties, [this, node](xmlAttr& attribute) -> Attribute {
            string_view name{toString(attribute.name)};
            string_view value =
                attributeValues.getOrCreate(&attribute, node->doc);
            return {name, value};
        });
}

Selection Model::findOr(xmlNode* node, Selection alternative) const {
    auto it = selectionMap.find(node);
    return (it != selectionMap.end()) ? (*it).second : alternative;
}

vector<ChildInfo> Model::newItemsVector(xmlNode* node) {
    return transformed<vector<ChildInfo>>(
        node->children, [](xmlNode& childNode) -> ChildInfo {
            switch (childNode.type) {
            case XML_ELEMENT_NODE:
                return ElementChild{toString(childNode.name)};
            case XML_TEXT_NODE:
                return TextChild{};
            default:
                throw logic_error{"No handler for this type of childNode"};
            }
        });
}
