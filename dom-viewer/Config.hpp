#ifndef CONFIG_HPP
#define CONFIG_HPP

namespace Config {
enum ColorHandles {
    cursor = 1,
    selectedItem,

};
}

namespace Config::KeyBindings {
constexpr char up = 'k';
constexpr char down = 'j';
constexpr char left = 'h';
constexpr char right = 'l';
constexpr char info = 'i';
constexpr char quit = 'q';
} // namespace Config::KeyBindings

#endif // CONFIG_HPP
