#include "Controller.hpp"

#include "Config.hpp"
#include "Model.hpp"

void Controller::handleInput(int input) {
    using namespace Config::KeyBindings;

    switch (input) {
    case up:
        model->selectPrevious();
        if (onSelectionUpdated) {
            onSelectionUpdated();
        }
        break;
    case down:
        model->selectNext();
        if (onSelectionUpdated) {
            onSelectionUpdated();
        }
        break;
    case right:
        if (!model->moveInto()) {
            // TODO cannot move deeper
        }
        if (onMoved) {
            onMoved();
        }
        break;
    case left:
        if (!model->moveOut()) {
            // TODO connot move outer
        }
        if (onMoved) {
            onMoved();
        }
        break;
    case quit:
        quitSet = true;
        break;
    default:
        logger.info("Unhandled input: ", input);
        break;
    }
}

bool Controller::isQuitSet() const { return quitSet; }
