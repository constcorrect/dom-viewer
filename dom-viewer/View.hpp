#pragma once

#include <cpp-utils/Logger.mpp>

#include <mycurses/Window.mpp>

class Model;
using ncurses::Window;

class View {
public:
    explicit View(Model* m, Window* w) : model{m}, window{w} {}
    virtual ~View() {}
    virtual void draw() = 0;

protected:
    Model* model;
    Window* window;
};
