#pragma once

#include <string>
#include <utility>

#include <cpp-utils/Logger.mpp>

#include <mycurses/Menu.mpp>
#include <mycurses/Window.mpp>

#include "Model.hpp"
#include "View.hpp"

struct Getters {
    std::vector<ChildInfo> (Model::*items)();
    int (Model::*index)() const;
};

class OwningMenu : public ncurses::Menu {
public:
    void setItems(const std::vector<std::string>& newItems) {
        if (!newItems.empty()) {
            items = newItems;
            ncurses::Menu::setItems([this]() {
                ncurses::MenuItems menuItems;
                for (auto const& item : items) {
                    menuItems.addItem(item.c_str(), "");
                }
                menuItems.commit();
                return menuItems;
            }());
        }
    }

private:
    std::vector<std::string> items;
};

class ListView : public View {
public:
    enum class Pos { Previous, Current, Next };
    explicit ListView(Model* m, ncurses::Window* w, Pos pos,
                      const char* logfileName = "view.log");
    void draw() override;
    void updateMenu();
    void updateSelection();

private:
    static std::vector<std::string>
    makeFormattedItems(const std::vector<ChildInfo>& items);
    void setWindowsAndFormatting();
    static void scrollToActualIndex(ncurses::Menu& menu, int index);

private:
    const Getters getters;
    OwningMenu menu;
    ncurses::Window derived;
    mutable FileLogger logger;
};
