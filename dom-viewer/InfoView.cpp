#include "InfoView.hpp"

#include <variant>

#include "Model.hpp"

using std::get;
using std::holds_alternative;
using std::string;
using std::to_string;

using ncurses::Position;
using ncurses::Size;

static Window initWin(Window& base) {
    auto [y, x] = base.maxyx();
    return base.derive(Size{y - 3, x - 3}, Position{2, 2});
}

InfoView::InfoView(Model* m, Window* w) : View{m, w}, derived{initWin(*w)} {}

void InfoView::draw() {
    window->clear();
    derived.clear();
    window->drawBox(0, 0);
    derived.addString([this]() -> string {
        NodeInfo infoVariant = model->selectedNodeFullInfo();
        if (holds_alternative<ElementNodeInfo>(infoVariant)) {
            ElementNodeInfo info = get<ElementNodeInfo>(infoVariant);
            string output =
                "Node type: Element, name: " + string(info.name) + "\n\n";
            for (auto& attr : info.attributes) {
                output += string(attr.name) + " : " + string(attr.value) + "\n";
            }
            return output;
        } else if (holds_alternative<TextNodeInfo>(infoVariant)) {
            TextNodeInfo info = get<TextNodeInfo>(infoVariant);
            string output = "Node type: Text, content length " +
                            to_string(info.content.size()) + "\n\nContent:\n" +
                            string(info.content);
            return output;
        } else {
            logger.warn(__FILE__, ": ", __FUNCTION__, ": unhandled condition");
            return "";
        }
    }());
    window->refresh();
}
