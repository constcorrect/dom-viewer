#include "ListView.hpp"

#include "Model.hpp"
#include <cpp-utils/Algorithms.mpp>

using std::get;
using std::holds_alternative;
using std::logic_error;
using std::string;
using std::vector;

using ncurses::Menu;
using ncurses::Position;
using ncurses::Size;

static Window initWin(Window& base) {
    auto [y, x] = base.maxyx();
    return base.derive(Size{y - 5, 38}, Position{3, 1});
}

Getters chooseGetters(ListView::Pos pos) {
    switch (pos) {
    case ListView::Pos::Previous: {
        return {&Model::itemsPrevious, &Model::previousSelectionIndex};
    }
    case ListView::Pos::Current: {
        return {&Model::items, &Model::selectionIndex};
    }
    case ListView::Pos::Next: {
        return {&Model::itemsNext, &Model::nextSelectionIndex};
    }
    }
}

ListView::ListView(Model* m, Window* w, Pos pos, const char* logfileName)
    : View{m, w}, getters{chooseGetters(pos)}, derived{initWin(*window)},
      logger{logfileName} {
    auto [y, x] = window->maxyx();
    window->drawBox(0, 0);
    window->move(2, 1);
    window->drawHorizontalLine('-', x - 2);
    updateMenu();
}

void ListView::draw() { window->refresh(); }

void ListView::updateMenu() {
    if (menu.isPosted()) {
        menu.unpost();
    }
    auto formattedItems = makeFormattedItems((model->*getters.items)());
    if (!formattedItems.empty()) {
        menu.setItems(formattedItems);
        setWindowsAndFormatting();
        menu.post();
        scrollToActualIndex(menu, (model->*getters.index)());
    }
}

void ListView::updateSelection() {
    scrollToActualIndex(menu, (model->*getters.index)());
}

vector<string> ListView::makeFormattedItems(const vector<ChildInfo>& items) {
    return transformed<vector<string>>(
        items, [](const ChildInfo& infoVariant) -> string {
            if (holds_alternative<ElementChild>(infoVariant)) {
                ElementChild info = get<ElementChild>(infoVariant);
                return string("Element node, name: ") + string(info.name);
            } else if (holds_alternative<TextChild>(infoVariant)) {
                return "Text node";
            } else {
                throw logic_error("Unknown alternative");
            }
        });
}

void ListView::setWindowsAndFormatting() {
    menu.setWin(*window);
    auto [y, x] = window->maxyx();
    // logger.info(y, " ", x);
    menu.setSubWin(derived);
    menu.setFormat(y - 5, 1);
    menu.setMark(" > ");
}

void ListView::scrollToActualIndex(Menu& menu, int index) {
    menu.driver(ncurses::Request::Item::first);
    while (index != 0) {
        menu.driver(ncurses::Request::Item::down);
        index--;
    }
}
