#pragma once

#include <functional>

#include <cpp-utils/Logger.mpp>

class Model;

class Controller {
public:
    Controller(Model* m) : model{m} {}
    void handleInput(int input);
    bool isQuitSet() const;
    bool isUpdateNeeded() const;

    enum class CallbackType { selectionUpdated, moved };

    template <typename Callable>
    void registerCallback(CallbackType type, Callable callback) {
        switch (type) {
        case CallbackType::selectionUpdated:
            onSelectionUpdated = callback;
            break;
        case CallbackType::moved:
            onMoved = callback;
            break;
        }
    }

private:
    std::function<void()> onSelectionUpdated;
    std::function<void()> onMoved; // TODO rename
    Model* model;
    bool quitSet{false};
    mutable FileLogger logger{"controller.log"};
};
