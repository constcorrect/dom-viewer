if [ -z ${1+x} ]; then build_mode='debug'; else build_mode="$1"; fi

build_dir="/tmp/dom-viewer-build"
[ ! -d "$build_dir" ] && mkdir -p "$build_dir"
build_options="build_mode=$build_mode
 libxml2_location=/usr/include/libxml2"
compiler='config.cxx=g++'

utils_build_options="build_mode=$build_mode
 use_modules=false
 $compiler"
b configure: cpp-utils/@$build_dir/cpp-utils/ $utils_build_options
echo 'use_modules=false' >> "$build_dir/cpp-utils/build/config.build"

libxml2_build_options="build_mode=$build_mode
 use_modules=false
 libxml2_location=/usr/include/libxml2
 $compiler"
b configure: libxml2-utils/@$build_dir/libxml2-utils/ $libxml2_build_options
echo 'use_modules=false' >> "$build_dir/libxml2-utils/build/config.build"
echo 'libxml2_location=/usr/include/libxml2' >> "$build_dir/libxml2-utils/build/config.build"

b configure: mycurses/@$build_dir/mycurses/ $build_options
b ./mycurses/@$build_dir/mycurses/ $build_options
b ./@$build_dir/ $build_options
